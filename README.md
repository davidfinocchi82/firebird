### Introduction

Firebird Ether Arbitrage is a Ruby trading system that does automatic long/short arbitrage between Ether exchanges.

### How It Works

Ether is still a new and inefficient market. Several Ether exchanges exist around the world and the bid/ask prices they propose can be briefly different from an exchange to another. The purpose of Firebird is to automatically profit from these temporary price differences while being market-neutral.

Here is a real example where an arbitrage opportunity exists between Kraken (long) and Poloniex (short):

![Spread Example](https://dl.dropboxusercontent.com/u/64436671/5863e750-8ab3-11e5-86fc-8f7bab6818df.png)

At the first vertical line, the spread between the exchanges is high so Firebird buys Kraken and short sells Poloniex. Then, when the spread closes (second vertical line), Firebird exits the market by selling Kraken and buying Poloniex back.

#### Advantages

Unlike other Ether arbitrage systems, Firebird doesn't sell but actually _short sells_ Ether on the short exchange. This feature offers two important advantages:

1. The strategy is always market-neutral: the Ether market's moves (up or down) don't impact the strategy returns. This removes a huge risk from the strategy. The Ether market could suddenly lose twice its value that this won't make any difference in the strategy returns.

2. The strategy doesn't need to transfer funds (USD or ETH) between Ether exchanges. The buy/sell and sell/buy trading activities are done in parallel on two different exchanges, independently. Advantage: no need to deal with transfer latency issues.

### Disclaimer

__USE THE SOFTWARE AT YOUR OWN RISK. YOU ARE RESPONSIBLE FOR YOUR OWN MONEY. PAST PERFORMANCE IS NOT NECESSARILY INDICATIVE OF FUTURE RESULTS.__

__THE AUTHORS AND ALL AFFILIATES ASSUME NO RESPONSIBILITY FOR YOUR TRADING RESULTS.