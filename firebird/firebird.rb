#millions of dollars 
#!/usr/bin/ruby

$LOAD_PATH << '.'
require 'byebug'
require 'lib/kraken.rb'
require 'lib/poloniex.rb'
require 'lib/bitfinex.rb'
require 'lib/coinbase.rb'
require 'lib/bitstamp.rb'

KRAKEN_API_KEY = 'k++og+H9WHoWZWfRpRwLSYWRj2hjgHPgw9TpZ/P2PvmxLNzNbEP25X0h'
KRAKEN_API_SECRET = 'ZdlBTyuLRV/fULtt5xDWbgu/94L8MB0kC0Y6YJp1V0ivJITy/2Bsz4nodSfTWCfwypRgIWLaGfVfjGRI7CAW/w=='

COINBASE_API_KEY = "81df185c26d2799318065b5ed4defc72"
COINBASE_API_SECRET = "6w9cnlu016oJuwbdrc24Xce1q3+Gic399fnnKAyOTKD/E9GQmvWwBST/QUooFrQqoHwIeVikqJxPgCyUMrvNnQ=="
COINBASE_API_PASS = "1jaso5oz54vgx51olqvx61dcxr"
COINBASE_USD_ID = "1065f32c-e6c6-4bd9-9b97-26c0aff5f33c"
COINBASE_BTC_ID = "dae808f8-09ea-4bab-9e38-0e4310cae326"

kraken_pair = "XBTUSD"
kraken_base_currency = "ZUSD" #XXBT for bitcoin, ZUSD for USD
kraken_trade_currency = "XXBT"
poloniex_pair = "USDT_BTC"
poloniex_base_currency = "USDT"
poloniex_trade_currency = "BTC"
bitfinex_pair = "BTCUSD"
bitfinex_base_currency = "USD"
bitfinex_trade_currency = "BTC"
coinbase_pair = "BTC-USD"
coinbase_base_currency = "USD"
coinbase_trade_currency = "BTC"
bitstamp_pair = "BTCUSD"
bitstamp_base_currency = "USD"
bitstamp_trade_currency = "BTC"
trade_amount = 10.00
win_amount = 0.7
poloniex_fee = 0.15
use_poloniex = true
kraken_fee = 0.16
use_kraken = true
bitfinex_fee = 0.1
use_bitfinex = true 
coinbase_fee = 0.15
use_coinbase = true
bitstamp_fee = 0.25
use_bitstamp = true
decimal_threshold_1 = 2
decimal_threshold_2 = 2
decimal_threshold_3 = 8
taker = 0.0003

Poloniex.setup do | config |
    config.key = '0WYO3HR4-NSM8DWZ0-I02YBIU3-G524HJO7'
    config.secret = 'b84b15da8296c0fecf5ffa3e920497473e98a5e30b60f387047f85fb518c662901dbdffc91b473ff996c04c31c37413945bdf5fbc5dda9ec37b860a3d4d18419'
end

Bitfinex::Client.configure do |conf|
    conf.secret = "5UjldXhPnjc6JKb5avWEglNLbKVTmlfvgS2qvbkAWYh"
    conf.api_key = "W44iFcfaZUq59ByVREb4CytnBCnNsERjKAWGsWOllTH"
end

Bitstamp.setup do |config|
  config.key = "XrSI0RNJfTTbZKzTLmEghIzXaixQAHGg"
  config.secret = "pfOaXoLmnOjBl3La2RQXZzUBjE1DQsmd"
  config.client_id = "232525"
end

def get_coinbase_bid(ticker)
    return ticker["bid"]
end

def get_coinbase_ask(ticker)
    return ticker["ask"]
end

def get_coinbase_balance(coinbase, id)
    return coinbase.account(id)["available"]
end

def get_kraken_balance(currency, kraken)
    kraken_balance_result = kraken.balance
    return kraken_balance_result[currency]
end

def get_kraken_bid(ticker)
    return ticker[ticker.first[0]]["b"][0]
end

def get_kraken_ask(ticker)
    return ticker[ticker.first[0]]["a"][0]
end

def get_kraken_bid_volume(ticker)
    return ticker[ticker.first[0]]["b"][2]
end

def get_kraken_ask_volume(ticker)
    return ticker[ticker.first[0]]["a"][2]
end

def get_kraken_spread(pair, kraken)
    return kraken.spread(pair)
end

def get_poloniex_balance(currency)
    poloniex_balance_result = Poloniex.balances
    return JSON.parse(poloniex_balance_result.body)["exchange"][currency]
end

def get_poloniex_bid(ticker, poloniex_pair)
    return JSON.parse(ticker.body)[poloniex_pair]["highestBid"]
end

def get_poloniex_ask(ticker, poloniex_pair)
    return JSON.parse(ticker.body)[poloniex_pair]["lowestAsk"]
end

def get_bitfinex_balance(currency, bitfinex)
    bitfinex_balance = bitfinex.balances
    return bitfinex_balance.select{|k,v| k["currency"] == currency.downcase}[0]["amount"]
end

def get_bitfinex_bid(ticker)
    return ticker["bid"]
end

def get_bitfinex_ask(ticker)
 return ticker["ask"]
end

def get_bitstamp_bid(ticker)
    return ticker.bid
end

def get_bitstamp_ask(ticker)
    return ticker.ask
end

def get_bitstamp_balance(currency)
    return Bitstamp.balance[currency]
end

def check_entry(spread, win_amount)
    if spread >= win_amount
        return true
    else
        return false
    end
end

def check_exit(spread, bid_two)
    if spread <= bid_two
        return true
    else
        return false    
    end
end

def check_balances(balance_one, trade_amount)
    if balance_one > trade_amount
        return true
    else
        puts "Not enough balance in the accounts\n"
        return false
    end
end

def create_kraken_order(kraken_pair, type, ordertype, price, volume, kraken, leverage)
    opts = {
        pair: kraken_pair,
        type: type,
        ordertype: ordertype,
        price: price,
        volume: volume,
        leverage: leverage
    }
    return kraken.add_order(opts)
end

bitfinex = Bitfinex::Client.new
kraken = Kraken::Client.new(KRAKEN_API_KEY, KRAKEN_API_SECRET)
coinbase = Coinbase::Exchange::Client.new(COINBASE_API_KEY, COINBASE_API_SECRET, COINBASE_API_PASS, product_id: coinbase_pair)

while true do
    search = true 
    order = false
    watch = false
    glitch = ""
    pair_exit = 0.0
    while search do
        begin
            kraken_ticker = kraken.ticker(kraken_pair)
            kraken_bid = get_kraken_bid(kraken_ticker).to_f.round(decimal_threshold_1)
            kraken_ask = get_kraken_ask(kraken_ticker).to_f.round(decimal_threshold_1)
            
            poloniex_ticker = Poloniex.ticker
            poloniex_bid = get_poloniex_bid(poloniex_ticker, poloniex_pair).to_f.round(decimal_threshold_1)
            poloniex_ask = get_poloniex_ask(poloniex_ticker, poloniex_pair).to_f.round(decimal_threshold_1)

            bitfinex_ticker = bitfinex.ticker(bitfinex_pair)
            bitfinex_bid = get_bitfinex_bid(bitfinex_ticker).to_f.round(decimal_threshold_1)
            bitfinex_ask = get_bitfinex_ask(bitfinex_ticker).to_f.round(decimal_threshold_1)

            coinbase_ticker = coinbase.ticker
            coinbase_bid = get_coinbase_bid(coinbase_ticker).to_f.round(decimal_threshold_1)
            coinbase_ask = get_coinbase_ask(coinbase_ticker).to_f.round(decimal_threshold_1)

            bitstamp_ticker = Bitstamp.ticker
            bitstamp_bid = get_bitstamp_bid(bitstamp_ticker).to_f.round(decimal_threshold_1)
            bitstamp_ask = get_bitstamp_ask(bitstamp_ticker).to_f.round(decimal_threshold_1)

            poloniex_kraken = (((kraken_bid - poloniex_ask) / poloniex_ask) * 100).round(decimal_threshold_1)
            bitfinex_kraken = (((kraken_bid - bitfinex_ask) / bitfinex_ask) * 100).round(decimal_threshold_1)
            coinbase_kraken = (((kraken_bid - coinbase_ask) / coinbase_ask) * 100).round(decimal_threshold_1)
            bitstamp_kraken = (((kraken_bid - bitstamp_ask) / bitstamp_ask) * 100).round(decimal_threshold_1)

            puts "Time: #{Time.now.inspect}\n"
            puts "Searching for opening\n"
            puts "Target is: #{win_amount}%"
            puts "Poloniex/Kraken: #{poloniex_kraken}%"
            puts "Bitfinex/Kraken: #{bitfinex_kraken}%"
            puts "Coinbase/Kraken: #{coinbase_kraken}%"
            puts "BitStamp/Kraken: #{bitstamp_kraken}%"
            
            if check_entry(poloniex_kraken, win_amount) and use_poloniex
                search = false
                order = true
                puts "Glitch in the Matrix found!!!\n\n"
                glitch = "poloniex_kraken"
            elsif check_entry(bitfinex_kraken, win_amount) and use_bitfinex
                search = false
                order = true
                puts "Glitch in the Matrix found!!!\n\n"
                glitch = "bitfinex_kraken"
            elsif check_entry(coinbase_kraken, win_amount) and use_coinbase
                search = false
                order = true
                puts "Glitch in the Matrix found!!!\n\n"
                glitch = "coinbase_kraken" 
            elsif check_entry(bitstamp_kraken, win_amount) and use_bitstamp
                search = false
                order = true
                puts "Glitch in the Matrix found!!!\n\n"
                glitch = "bitstamp_kraken"    
            else
                puts "No entries found\n\n"
            end
        rescue => e
            puts "Error happended somewhere: #{e}"
            sleep 5
            retry
        end
        if search == true then sleep 1 end
    end

    while order do 
        begin 
            kraken_ticker = kraken.ticker(kraken_pair)
            kraken_bid = get_kraken_bid(kraken_ticker).to_f.round(decimal_threshold_1)
            kraken_ask = get_kraken_ask(kraken_ticker).to_f.round(decimal_threshold_1)
            kraken_balance = get_kraken_balance(kraken_base_currency, kraken).to_f
            puts "Kraken balance #{kraken_base_currency}: #{kraken_balance}\n"

            poloniex_ticker = Poloniex.ticker
            poloniex_bid = get_poloniex_bid(poloniex_ticker, poloniex_pair).to_f.round(decimal_threshold_1)
            poloniex_ask = get_poloniex_ask(poloniex_ticker, poloniex_pair).to_f.round(decimal_threshold_1)
            poloniex_balance = get_poloniex_balance(poloniex_base_currency).to_f
            puts "Poloniex balance #{poloniex_base_currency}: #{poloniex_balance}\n"

            bitfinex_ticker = bitfinex.ticker(bitfinex_pair)
            bitfinex_bid = get_bitfinex_bid(bitfinex_ticker).to_f.round(decimal_threshold_1)
            bitfinex_ask = get_bitfinex_ask(bitfinex_ticker).to_f.round(decimal_threshold_1)
            bitfinex_balance = get_bitfinex_balance(bitfinex_base_currency, bitfinex).to_f
            puts "Bitfinex balance #{bitfinex_base_currency}: #{bitfinex_balance}\n"

            coinbase_ticker = coinbase.ticker
            coinbase_bid = get_coinbase_bid(coinbase_ticker).to_f.round(decimal_threshold_1)
            coinbase_ask = get_coinbase_ask(coinbase_ticker).to_f.round(decimal_threshold_1)
            coinbase_balance = get_coinbase_balance(coinbase, COINBASE_USD_ID)
            puts "Coinbase balance #{coinbase_base_currency}: #{coinbase_balance}\n"

            bitstamp_ticker = Bitstamp.ticker
            bitstamp_bid = get_bitstamp_bid(bitstamp_ticker).to_f.round(decimal_threshold_1)
            bitstamp_ask = get_bitstamp_ask(bitstamp_ticker).to_f.round(decimal_threshold_1)
            bitstamp_balance = get_bitstamp_balance("usd_available")
            puts "Bitstamp balance #{bitstamp_base_currency}: #{bitstamp_balance}\n\n"

            if glitch == "poloniex_kraken"
                if check_balances(poloniex_balance, trade_amount)
                    #place kraken order
                    kraken_order = create_kraken_order(kraken_pair, 'sell', 'limit', (kraken_ask - (kraken_ask * taker)).round(decimal_threshold_3), (trade_amount/kraken_bid).round(decimal_threshold_3), kraken, 2)
                    puts "Kraken Short Order: #{kraken_order}\n"
                    #place poloniex order
                    poloniex_order = Poloniex.buy(poloniex_pair, (poloniex_bid + (poloniex_bid * taker)).round(decimal_threshold_3), (trade_amount/kraken_bid).round(decimal_threshold_3))
                    puts "Poloniex Order: #{poloniex_order}\n\n"
                    pair_exit = -(poloniex_fee + kraken_fee)
                    order = false
                    watch = true
                    sleep 300
                else
                    search = true 
                    order = false
                end
            elsif glitch == "bitfinex_kraken"
                if check_balances(bitfinex_balance, trade_amount)
                    #place kraken order
                    kraken_order = create_kraken_order(kraken_pair, 'sell', 'limit', (kraken_ask - (kraken_ask * taker)).round(decimal_threshold_3), (trade_amount/kraken_bid).round(decimal_threshold_3), kraken, 2)
                    puts "Kraken Short Order: #{kraken_order}\n"
                    #place bitfinex order
                    bitfinex_order = bitfinex.new_order(bitfinex_pair, (trade_amount/kraken_bid).round(decimal_threshold_3), "limit", "buy", (bitfinex_bid + (bitfinex_bid * taker)).round(decimal_threshold_3), nil)
                    puts "Bitfinex Order: #{bitfinex_order}\n\n"
                    pair_exit = -(bitfinex_fee + kraken_fee)
                    order = false
                    watch = true
                    sleep 300
                else
                    search = true 
                    order = false
                end
            elsif glitch == "coinbase_kraken"
                if check_balances(coinbase_balance, trade_amount)
                    #place kraken order
                    kraken_order = create_kraken_order(kraken_pair, 'sell', 'limit', (kraken_ask - (kraken_ask * taker)).round(decimal_threshold_3), (trade_amount/kraken_bid).round(decimal_threshold_3), kraken, 2)
                    puts "Kraken Short Order: #{kraken_order}\n"
                    #place coinbase order
                    coinbase_order = coinbase.bid((trade_amount/kraken_bid).round(decimal_threshold_3), (coinbase_bid + (coinbase_bid * taker)).round(decimal_threshold_2))
                    puts "Coinbase Order: #{coinbase_order}\n\n"
                    pair_exit = -(coinbase_fee + kraken_fee)
                    order = false
                    watch = true
                    sleep 300
                else
                    search = true 
                    order = false
                end
            elsif glitch == "bitstamp_kraken"
                if check_balances(poloniex_balance, trade_amount)
                    #place kraken order
                    kraken_order = create_kraken_order(kraken_pair, 'sell', 'limit', (kraken_ask - (kraken_ask * taker)).round(decimal_threshold_3), (trade_amount/kraken_bid).round(decimal_threshold_3), kraken, 2)
                    puts "Kraken Short Order: #{kraken_order}\n"
                    #place bitstamp order
                    bitstamp_order = Bitstamp.orders.buy(amount: (trade_amount/kraken_bid).round(decimal_threshold_3), price: (bitstamp_bid + (bitstamp_bid * taker)).round(decimal_threshold_3))
                    puts "Bitstamp Order: #{bitstamp_order}\n\n"
                    pair_exit = -0.10
                    order = false
                    watch = true
                    sleep 300
                else
                    search = true 
                    order = false
                end
            end
        rescue => e
            puts "Error happended somewhere: #{e}"
            sleep 5
            retry
        end
    end

    while watch do 
        begin
            kraken_ticker = kraken.ticker(kraken_pair)
            kraken_bid = get_kraken_bid(kraken_ticker).to_f.round(decimal_threshold_1)
            kraken_ask = get_kraken_ask(kraken_ticker).to_f.round(decimal_threshold_1)
            
            poloniex_ticker = Poloniex.ticker
            poloniex_bid = get_poloniex_bid(poloniex_ticker, poloniex_pair).to_f.round(decimal_threshold_1)
            poloniex_ask = get_poloniex_ask(poloniex_ticker, poloniex_pair).to_f.round(decimal_threshold_1)
            poloniex_balance = get_poloniex_balance(poloniex_trade_currency).to_f

            bitfinex_ticker = bitfinex.ticker(bitfinex_pair)
            bitfinex_bid = get_bitfinex_bid(bitfinex_ticker).to_f.round(decimal_threshold_1)
            bitfinex_ask = get_bitfinex_ask(bitfinex_ticker).to_f.round(decimal_threshold_1)
            bitfinex_balance = get_bitfinex_balance(bitfinex_trade_currency, bitfinex).to_f

            coinbase_ticker = coinbase.ticker
            coinbase_bid = get_coinbase_bid(coinbase_ticker).to_f.round(decimal_threshold_1)
            coinbase_ask = get_coinbase_ask(coinbase_ticker).to_f.round(decimal_threshold_1)
            coinbase_balance = get_coinbase_balance(coinbase, COINBASE_BTC_ID)

            bitstamp_ticker = Bitstamp.ticker
            bitstamp_bid = get_bitstamp_bid(bitstamp_ticker).to_f.round(decimal_threshold_1)
            bitstamp_ask = get_bitstamp_ask(bitstamp_ticker).to_f.round(decimal_threshold_1)
            bitstamp_balance = get_bitstamp_balance("btc_available")

            poloniex_kraken = (((kraken_bid - poloniex_ask) / poloniex_ask) * 100).round(decimal_threshold_1)
            bitfinex_kraken = (((kraken_bid - bitfinex_ask) / bitfinex_ask) * 100).round(decimal_threshold_1)
            coinbase_kraken = (((kraken_bid - coinbase_ask) / coinbase_ask) * 100).round(decimal_threshold_1)
            bitstamp_kraken = (((kraken_bid - bitstamp_ask) / bitstamp_ask) * 100).round(decimal_threshold_1)

            puts "Time: #{Time.now.inspect}\n"
            puts "Monitoring #{glitch} trade\n"
            puts "Target is: #{pair_exit}%"
            puts "Poloniex/Kraken: #{poloniex_kraken}%"
            puts "Bitfinex/Kraken: #{bitfinex_kraken}%"
            puts "Coinbase/Kraken: #{coinbase_kraken}%\n"
            puts "Bitstamp/Kraken: #{bitstamp_kraken}%\n\n"

            if glitch == "poloniex_kraken"
                if check_exit(poloniex_kraken, pair_exit)
                    #close kraken order
                    kraken_order = create_kraken_order(kraken_pair, 'buy', 'limit', (kraken_bid + (kraken_bid * taker)).round(decimal_threshold_3), 0, kraken, 2)
                    puts "Closing Kraken Order: #{kraken_order}\n"
                    #close poloniex order
                    poloniex_order = Poloniex.sell(poloniex_pair, (poloniex_ask - (poloniex_ask * taker)).round(decimal_threshold_3), poloniex_balance)
                    puts "Closing Poloniex Order: #{poloniex_order}\n"
                    watch = false
                    sleep 300
                end
            elsif glitch == "bitfinex_kraken"
                if check_exit(bitfinex_kraken, pair_exit)
                    # close kraken order
                    kraken_order = create_kraken_order(kraken_pair, 'buy', 'limit', (kraken_bid + (kraken_bid * taker)).round(decimal_threshold_3), 0, kraken, 2)
                    puts "Closing Kraken Order: #{kraken_order}\n"
                    #close bitfinex order
                    bitfinex_order = bitfinex.new_order(bitfinex_pair, bitfinex_balance, "limit", "sell", (bitfinex_ask - (bitfinex_ask * taker)).round(decimal_threshold_3), nil)
                    puts "Bitfinex Order: #{bitfinex_order}\n"
                    watch = false
                    sleep 300
                end
            elsif glitch == "coinbase_kraken"
                if check_exit(coinbase_kraken, pair_exit)
                    #close kraken order
                    kraken_order = create_kraken_order(kraken_pair, 'buy', 'limit', (kraken_bid + (kraken_bid * taker)).round(decimal_threshold_3), 0, kraken, 2)
                    puts "Closing Kraken Order: #{kraken_order}\n"
                    #close coinbase order
                    coinbase_order = coinbase.ask(coinbase_balance, (coinbase_ask - (coinbase_ask * taker)).round(decimal_threshold_2))
                    puts "Coinbase Order: #{coinbase_order}\n\n"
                    watch = false
                    sleep 300
                end
            elsif glitch == "bitstamp_kraken"
                if check_exit(bitstamp_kraken, pair_exit)
                    #close kraken order
                    kraken_order = create_kraken_order(kraken_pair, 'buy', 'limit', (kraken_bid + (kraken_bid * taker)).round(decimal_threshold_3), 0, kraken, 2)
                    puts "Closing Kraken Order: #{kraken_order}\n"
                    #close bitstamp order
                    bitstamp_order = Bitstamp.orders.sell(amount: bitstamp_balance, price: (bitstamp_ask  - (bitstamp_ask * taker)).round(decimal_threshold_3))
                    puts "Bitstamp Order: #{bitstamp_order}\n\n"
                    watch = false
                    sleep 300
                end
            end
        rescue => e
            puts "Error happended somewhere: #{e}"
            sleep 5
            retry
        end
        if watch == true then sleep 1 end
    end
end