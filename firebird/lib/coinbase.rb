$LOAD_PATH << '.'
require "bigdecimal"
require "json"
require "uri"
require "net/http"
require "em-http-request"
require "faye/websocket"

require "lib/coinbase/errors"
require "lib/coinbase/api_object"
require "lib/coinbase/api_response"
require "lib/coinbase/api_client.rb"
require "lib/coinbase/adapters/net_http.rb"
require "lib/coinbase/adapters/em_http.rb"
require "lib/coinbase/client"
require "lib/coinbase/websocket"

module Coinbase
  # Coinbase Exchange module
  module Exchange
  end
end
